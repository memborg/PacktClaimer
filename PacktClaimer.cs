using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
namespace PacktClaimer {

    class PacktClaimer {

        public static void Main(string[] args) {
            var baseUrl = "https://www.packtpub.com";

            var username = args[0];
            var pw = args[1];

            var url = baseUrl + "/packt/offers/free-learning";
            var formParams = string.Format("email={0}&password={1}&op=Login&form_id=packt_user_login_form&form_build_id=form-f07f20a452634918421c612d5aee2408", username, pw);
            var userAgent = "Googlebot/2.1 (+http://www.google.com/bot.html)";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            request.AllowAutoRedirect = true;
            request.UserAgent = userAgent;
            request.CookieContainer = new CookieContainer();
            byte[] bytes = Encoding.UTF8.GetBytes(formParams);
            request.ContentLength = bytes.Length;
            using (Stream os = request.GetRequestStream())
            {
                os.Write(bytes, 0, bytes.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            var cookies = request.CookieContainer;

            Console.WriteLine(response.StatusCode);

            var text = string.Empty;
            using (var reader = new StreamReader(response.GetResponseStream())) {
                text = reader.ReadToEnd();
            }
            response.Close();

            var regex = new Regex(@"/freelearning-claim/\d+/\d+");
            var match = regex.Match(text);

            if(match.Success) {
                var path = match.Value;

                url = baseUrl + path;
                Console.WriteLine(url);
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.AllowAutoRedirect = true;
                request.UserAgent = userAgent;
                request.CookieContainer = cookies;
                response = (HttpWebResponse)request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    File.WriteAllText("dump.html", reader.ReadToEnd());
                }
                response.Close();
            }

        }
    }
}
